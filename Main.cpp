//============================================================================
// Name        : FudamentalCPPCanonicalStringClass.cpp
// Author      : Ridit
// Version     :
// Copyright   : @Ridit...
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include "MyString.h"
using namespace std;

int main() {
	MyString name("Er Ridit");
	MyString title(" Mukhopadhyay");
	MyString name1("Er Ridit");

	if(name == title){
		cout <<"These two strings are equal"<<endl;
	}
	else {
		cout <<"These two strings are not equal"<<endl;
	}

	if(name == name1){
			cout <<"These two strings are equal"<<endl;
	}
	else {
			cout <<"These two strings are not equal"<<endl;
	}

	name += title;

	name1 += " says CPP is fun...";


	cout<<name.c_str()<<endl;

	cout <<name1.c_str()<<endl;


}
