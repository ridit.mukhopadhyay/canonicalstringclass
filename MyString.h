/*
 * MyString.h
 *
 *  Created on: 22-Jan-2022
 *      Author: ridit
 */

#ifndef MYSTRING_H_
#define MYSTRING_H_

#include <cstddef>
#include <string.h>
#include <iostream>
#include <boost/uuid/uuid.hpp>            // uuid class
#include <boost/uuid/uuid_generators.hpp> // generators
#include <boost/uuid/uuid_io.hpp>         // streaming operators etc.
#include "StrUtility.h"

class MyString {
	char* data;
	unsigned int length;
	boost::uuids::uuid id;
public:
	MyString();
	MyString(const char* inStr);

	virtual ~MyString();
	MyString(const MyString& other);
	MyString& operator=(const MyString& other);
	MyString& operator+=(const MyString& other);
	MyString& operator+=(const char* inStr);
	bool operator==(const MyString& other);
	bool operator==(char* ptr);

	inline char* c_str(){
		return data;
	}
	inline unsigned int getLength(){
		return length;
	}


	inline boost::uuids::uuid getId(){
		return id;
	}
};

#endif /* MYSTRING_H_ */
