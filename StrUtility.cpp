/*
 * StrUtility.cpp
 *
 *  Created on: Jan 17, 2022
 *      Author: som
 */

#include "StrUtility.h"

StrUtility::StrUtility() {
	// TODO Auto-generated constructor stub

}

StrUtility::~StrUtility() {
	// TODO Auto-generated destructor stub
}

void* StrUtility::memcpy(void *dst, const void *src, size_t len)
 {
         size_t i;

         /*
          * memcpy does not support overlapping buffers, so always do it
          * forwards. (Don't change this without adjusting memmove.)
          *
          * For speedy copying, optimize the common case where both pointers
          * and the length are word-aligned, and copy word-at-a-time instead
          * of byte-at-a-time. Otherwise, copy by bytes.
          *
          * The alignment logic below should be portable. We rely on
          * the compiler to be reasonably intelligent about optimizing
          * the divides and modulos out. Fortunately, it is.
          */

         if ((uintptr_t)dst % sizeof(long) == 0 &&
             (uintptr_t)src % sizeof(long) == 0 &&
             len % sizeof(long) == 0) {

                 long *d = dst;
                 const long *s = src;

                 for (i=0; i<len/sizeof(long); i++) {
                         d[i] = s[i];
                 }
         }
         else {
                 char *d = dst;
                 const char *s = src;

                 for (i=0; i<len; i++) {
                         d[i] = s[i];
                 }
         }

         return dst;
 }

size_t StrUtility::strlen(const char *str) {
        const char *s;

        for (s = str; *s; ++s)
                ;
        return (s - str);
}

char* StrUtility::strcat(char *dest, const char *src){
	    char *rdest = dest;
	    while (*dest)
	      dest++;
	    while (*dest++ = *src++)
	      ;
	    return rdest;
}

bool StrUtility::isEqual(char *str1, char *str2) {
	if(strlen(str1) != strlen(str2)){
		return false;
	}
	int length = strlen(str1);

	while(*str1 != '\0'){
		if(*str1 != *str2){
			return false;
		}
		str1++;
		str2++;
	}
	return true;
}
