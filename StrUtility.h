/*
 * StrUtility.h
 *
 *  Created on: Jan 17, 2022
 *      Author: som
 */

#ifndef STRUTILITY_H_
#define STRUTILITY_H_

#include <cstddef>
#include <iostream>

using namespace std;

class StrUtility {
public:
	static void *memcpy(void *dst, const void *src, size_t len);

	static size_t strlen(const char *str);

	static char* strcat(char *dest, const char *src);

	static bool isEqual(char* str1,char* str2);

	StrUtility();
	virtual ~StrUtility();
};

#endif /* STRUTILITY_H_ */
