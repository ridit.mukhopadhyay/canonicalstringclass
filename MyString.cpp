/*
 * MyString.cpp
 *
 *  Created on: 22-Jan-2022
 *      Author: ridit
 */

#include "MyString.h"

MyString::MyString() {
	// TODO Auto-generated constructor stub
	data = NULL;
	length = 0;
	id = boost::uuids::nil_uuid();
}

MyString::MyString(const char* inStr){
	unsigned l=StrUtility::strlen(inStr);
	data=new char[l+1];
	StrUtility::memcpy(data,inStr,l+1);
	length=l;
	id = boost::uuids::random_generator()();
}

MyString::~MyString() {
	// TODO Auto-generated destructor stub
	if(data != NULL){
		cout<<"deleting... data from object" << id <<endl;
		delete[]data;
		data = NULL;
	}
}

MyString::MyString(const MyString &other) {
	// TODO Auto-generated constructor stub
	int l = other.length;
	data = new char[l+1];
	StrUtility::memcpy(data, other.data, l+1);
	length = l;
	id = boost::uuids::random_generator()();

}

MyString& MyString::operator=(const MyString &other) {
	if(this == &other){
		return *this;
	}
	int l = other.length;
	data = new char[l+1];
	StrUtility::memcpy(data, other.data, l+1);
	length= l;
	id = boost::uuids::random_generator()();

	return *this;

}

MyString& MyString::operator+=(const MyString &other) {
	int l = length + other.length;
	char* temp = new char[l+1];
	StrUtility::memcpy(temp, data, length);
	delete[] data;
	data = NULL;
	StrUtility::strcat(temp, other.data);

	data = temp;
	length = StrUtility::strlen(data);
	return *this;

}
MyString& MyString::operator+=(const char* inStr) {
	int l = length + StrUtility::strlen(inStr);
	char* temp = new char[l+1];
	StrUtility::memcpy(temp, data, length);
	delete[] data;
	data = NULL;
	StrUtility::strcat(temp,inStr);
	data = temp;
	length = StrUtility::strlen(data);
	return *this;

}

bool MyString::operator ==(const MyString &other) {
	return StrUtility::isEqual(this->data, other.data);
}

bool MyString::operator ==(char *ptr) {
	return StrUtility::isEqual(this->data, ptr);

}
